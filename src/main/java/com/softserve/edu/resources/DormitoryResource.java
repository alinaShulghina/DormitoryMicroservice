package com.softserve.edu.resources;

import com.softserve.edu.api.RequestDao;
import com.softserve.edu.api.ResponseDao;
import com.softserve.edu.core.DormitoryService;
import com.softserve.edu.db.Dormitory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.List;


/**
 * @author Alina Shulhina
 */

@Path("/dormitories")
@Produces(MediaType.APPLICATION_JSON)
public class DormitoryResource {

    private DormitoryService dormitoryService;

    public DormitoryResource(DormitoryService dormitoryService) {
        this.dormitoryService = dormitoryService;
    }

    @GET
    public Response getAllDormitories() {
        List<Dormitory> dormitories = dormitoryService.findAll();
        return Response.status(Response.Status.OK).entity(dormitories).build();
    }

    @GET
    @Path("/{id}")
    public Response getDormitory(@PathParam("id") Integer id) {
        Dormitory dormitory = dormitoryService.findById(id);
        return Response.status(Response.Status.OK).entity(dormitory).build();
    }

    @GET
    @Path("/days")
    public Response getDays() {
        RequestDao requestDao = new RequestDao();
        requestDao.setUniversityId(1);
        requestDao.setStartDate(LocalDate.of(2018, 1, 28));
        requestDao.setEndDate(LocalDate.of(2018, 2, 28));
        return Response.status(Response.Status.OK).entity(dormitoryService.hasAvailbaleSeats(requestDao)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addNewDormitory(Dormitory dormitory) {
        System.out.println(dormitory);
        return Response.status(Response.Status.OK).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDormitory(@PathParam("id") Integer id, Dormitory dormitory) {
        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteDormitory(@PathParam("id") Integer id) {
        return Response.status(Response.Status.OK).entity(dormitoryService.deleteDormitory(id)).build();
    }
}
