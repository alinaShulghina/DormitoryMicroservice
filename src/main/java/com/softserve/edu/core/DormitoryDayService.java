package com.softserve.edu.core;

import com.softserve.edu.db.DormitoryDay;
import com.softserve.edu.db.dao.DormitoryDayDao;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

import java.util.List;

/**
 * @author Alina Shulhina
 */
public abstract class DormitoryDayService {

    @CreateSqlObject
    abstract DormitoryDayDao dormitoryDayDao();

    public List<DormitoryDay> findAll(){
        return dormitoryDayDao().findAll();
    }


}
