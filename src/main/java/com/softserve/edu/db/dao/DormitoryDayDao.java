package com.softserve.edu.db.dao;

import com.softserve.edu.db.DormitoryDay;
import com.softserve.edu.db.mapper.DormitoryDayMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Alina Shulhina
 */
@RegisterMapper(DormitoryDayMapper.class)
public interface DormitoryDayDao {

    @SqlQuery("SELECT * FROM dormitory_day")
    List<DormitoryDay> findAll();

    @SqlQuery("SELECT * FROM dormitory_day AS dd INNER JOIN dormitory AS d" +
            "ON dd.dormitoryId = d.id WHERE d.universityId = :id " +
            "AND dd. date in (:startDate, :endDate)" +
            "AND dd.availableSeats > 0")
    List<DormitoryDay> findByUniversityId(@Bind("id") Integer universityId,
                                         @Bind("startDate") LocalDate startDate,
                                         @Bind("endDate") LocalDate endDate);
}
