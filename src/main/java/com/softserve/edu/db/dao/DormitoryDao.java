package com.softserve.edu.db.dao;

import com.softserve.edu.db.Dormitory;
import com.softserve.edu.db.mapper.DormitoryMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

/**
 * @author Alina Shulhina
 */
@RegisterMapper(DormitoryMapper.class)
public interface DormitoryDao {

    @SqlQuery("SELECT * FROM dormitory")
    List<Dormitory> findAll();

    @SqlQuery("SELECT * FROM dormitory WHERE id = :id")
    Dormitory findById(@Bind("id") Integer id);

    @SqlQuery("SELECT * FROM dormitory WHERE universityId = :id")
    List<Dormitory> findByUniversityId(@Bind("id") Integer id);

    Dormitory updateDormitory(Dormitory dormitory);

    @SqlUpdate("DELETE FROM dormitory WHERE id = :id")
    int deleteDormitory(@Bind("id") Integer id);
}
