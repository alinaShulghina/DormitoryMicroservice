package com.softserve.edu.db.mapper;

import com.softserve.edu.db.Dormitory;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Alina Shulhina
 */
public class DormitoryMapper implements ResultSetMapper<Dormitory> {

    private static final String ID = "id";

    private static final String UNIVERSITY_ID = "universityId";

    private static final String CAPACITY = "capacity";

    @Override
    public Dormitory map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new Dormitory(resultSet.getInt(ID), resultSet.getInt(UNIVERSITY_ID), resultSet.getInt(CAPACITY));
    }
}
