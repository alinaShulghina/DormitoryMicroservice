package com.softserve.edu.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Alina Shulhina
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Dormitory {

    private Integer id;

    private Integer universityId;

    private Integer capacity;

}
