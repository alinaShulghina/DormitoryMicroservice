package com.softserve.edu;

import com.softserve.edu.core.DormitoryDayService;
import com.softserve.edu.core.DormitoryService;
import com.softserve.edu.resources.DormitoryDayResource;
import com.softserve.edu.resources.DormitoryResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.skife.jdbi.v2.DBI;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DormitoryApplication extends Application<DormitoryConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DormitoryApplication().run(args);
    }

    @Override
    public String getName() {
        return "Dormitory";
    }

    @Override
    public void initialize(final Bootstrap<DormitoryConfiguration> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<DormitoryConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(DormitoryConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final DormitoryConfiguration configuration,
                    final Environment environment) {
        DataSource dataSource = configuration.getDataSourceFactory().build(environment.metrics(), "sql");
        try (Connection connection = dataSource.getConnection()) {
            Liquibase migrator = new Liquibase("migrations/db-changelog-master.xml",
                    new ClassLoaderResourceAccessor(),
                    new JdbcConnection(connection));
            migrator.dropAll();
            migrator.update("");
        } catch (LiquibaseException e) {
            System.out.println("Liquibase");
        } catch (SQLException e) {
            System.out.println("SQL");
        }

        DBI dbi = new DBI(dataSource);

        DormitoryResource dormitoryResource = new DormitoryResource(dbi.onDemand(DormitoryService.class));
        environment.jersey().register(dormitoryResource);
    }

}
