package com.softserve.edu.db.mapper;

import com.softserve.edu.db.DormitoryDay;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * @author Alina Shulhina
 */
public class DormitoryDayMapper implements ResultSetMapper<DormitoryDay> {

    private static final String ID = "id";

    private static final String DORMITORY_ID = "dormitoryId";

    private static final String DATE = "date";

    private static final String AVAILABLE_SEATS = "availableSeats";

    @Override
    public DormitoryDay map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        return new DormitoryDay(resultSet.getLong(ID),
                resultSet.getInt(DORMITORY_ID),
                resultSet.getDate(DATE).toLocalDate(),
                resultSet.getInt(AVAILABLE_SEATS));
    }
}
