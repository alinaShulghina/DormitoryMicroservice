package com.softserve.edu.resources;

import com.softserve.edu.core.DormitoryDayService;
import org.skife.jdbi.v2.sqlobject.Bind;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.awt.*;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Alina Shulhina
 */
@Path("/days")
@Produces(APPLICATION_JSON)
public class DormitoryDayResource {

    private DormitoryDayService dormitoryDayService;

    public DormitoryDayResource(DormitoryDayService dormitoryDayService) {
        this.dormitoryDayService = dormitoryDayService;
    }

    @GET
    public Response findAll() {
        return Response.status(Response.Status.OK).entity(dormitoryDayService.findAll()).build();
    }
}
