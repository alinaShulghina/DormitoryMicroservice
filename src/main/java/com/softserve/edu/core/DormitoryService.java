package com.softserve.edu.core;

import com.softserve.edu.api.RequestDao;
import com.softserve.edu.db.Dormitory;
import com.softserve.edu.db.DormitoryDay;
import com.softserve.edu.db.dao.DormitoryDao;
import com.softserve.edu.db.dao.DormitoryDayDao;
import org.skife.jdbi.v2.sqlobject.CreateSqlObject;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

/**
 * @author Alina Shulhina
 */
public abstract class DormitoryService {

    @CreateSqlObject
    abstract DormitoryDao dormitoryDao();

    @CreateSqlObject
    abstract DormitoryDayDao dormitoryDayDao();

    public List<Dormitory> findAll() {
        return dormitoryDao().findAll();
    }

    public Dormitory findById(Integer id) {
        Dormitory dormitory = dormitoryDao().findById(id);
        if (Objects.isNull(dormitory)) {
            throw new WebApplicationException("Dormitory not found!", Response.Status.NOT_FOUND);
        }
        return dormitory;
    }

    public String deleteDormitory(Integer id) {
        int result = dormitoryDao().deleteDormitory(id);
        if (result == 0) {
            throw new WebApplicationException("Dormitory not found!", Response.Status.NOT_FOUND);
        } else if (result == 1) {
            return "Successfully deleted!";
        } else {
            throw new WebApplicationException("Unexpected error!", Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public List<DormitoryDay> hasAvailbaleSeats(RequestDao requestDao) {
        List<DormitoryDay> dormitoryDays = dormitoryDayDao().findByUniversityId(requestDao.getUniversityId(),
                requestDao.getStartDate(), requestDao.getEndDate());
        return dormitoryDays;
    }

}
