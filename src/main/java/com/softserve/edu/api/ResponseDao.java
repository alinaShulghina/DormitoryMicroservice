package com.softserve.edu.api;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Alina Shulhina
 */
@Getter
@Setter
public class ResponseDao implements Serializable {

    private static final Long SERIAL_ID = -123456789L;

    private Boolean hasAvailablePlaces;
}
