package com.softserve.edu.db;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

/**
 * @author Alina Shulhina
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DormitoryDay {

    private Long id;

    private Integer dormitoryId;

    private LocalDate date;

    private Integer availableSeats;
}
