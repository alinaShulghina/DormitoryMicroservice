package com.softserve.edu.api;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author Alina Shulhina
 */
@Getter
@Setter
public class RequestDao implements Serializable {

    private static final Long SERIAL_ID = 123456789L;

    private Integer universityId;

    private LocalDate startDate;

    private LocalDate endDate;
}
